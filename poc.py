#! /usr/bin/env python3

"""
We send emails out and each recipient interacts with the email (or not).
We log the emails and the recipient events.

Using Mitre10 prod data.

MailLog (logs) is the parent which is mainly useful for identifying the person
receiving the mail (logs.U3L_ReferenceID). N = 59,385

MailEvent (events) is all the interactions with the recipients. N = 236,118
i.e. about 4 interactions per message.

logs.id = events.messageid

logs:
id - a unique message

campaignid - id for campaign. Each campaign will have lots of emails/messages sent out and lots of interactions
mailoutid - id for specific email being sent to multiple people
u3l_referenceid - human recipient. So this table has one record per person receiving an email
statusmessage - e.g. ACCEPT (57,714 i.e. nearly all), messages about disabled message boxes etc

events:
messageid - link to unique messages in logs table. A single email sent to a person is one message.
event e.g. ConsecutiveRead (83,398), Scheduled (59,389), Delivered (57,695), LinkClicked (17,238) etc

* Read - recipient has read the email once. This is the time that the email was first read, a read counts as:
  downloading images in the email, or clicking a tracked link in the email
* ConsecutiveRead - recipient has read the email more than once. One row for each additional time they read the email,
  so if they read an email 5 times, they would have 1x Read event and 4x ConsecutiveRead events.
* Delivered - email has been received by the recipient, but not necessarily opened or interacted with.
  Also possible that the recipient deleted the email without opening it.
* LinkClicked - recipient clicked on one or more links in the email, (but) this includes the Opt Out link.
  
* PermanentFailure - email has bounced once. The recipient will be GNA’d if you have a PermanentFailure from 3 consecutive mailouts.
* TransientFailure - email could not be delivered, but this covers all other cases that are not GNA.
* OptedOut - recipient has opted out of the email – the recipient opted out of all emails for the account,
  not just updating communication preferences for specific messages.
* MarkedAsSpam - user clicked the mark this message as spam button in Hotmail or Gmail.
* Scheduled - number of emails that have been scheduled but yet not sent. No recipient interactions here.
* Cancelled - these emails have been scheduled, but not sent. Like the above, this metric doesn’t include any recipient interaction.

Looking at data from the point of view of campaigns we need to filter by campaignid

Looking at the recipients we need to join events to logs so we can filter by u3l_referenceid.
"""

from collections import Counter, defaultdict
import os
from pathlib import Path
from random import sample

from qat import db, settings

DB_NAME = 'engage'

def ingest_mail_csvs(ingest_path):
    con, cur = db.Pg.local(DB_NAME)
    fnames = [fname for fname in os.listdir(ingest_path) if fname.startswith('Mail')]
    for fname in fnames:
        fpath = Path(ingest_path) / fname
        if fname.startswith('MailLog'):
            dest_tblname = 'logs'
            dest_fields = ['ID', 'RowID', 'CampaignID', 'MailoutID',
                'U3L_ReferenceID', 'Status', 'StatusMessage', 'ReadDate',
                'BounceDate', 'OptOutDate', 'HasClicked', 'LastModifiedDate',
                'InstanceID', 'Version', 'DeliveryDate']
            ## 603EB765-C884-4581-8011-08D4B33F3AA1;90;46CA3CB9-86BC-466B-8C0A-08D4B323CE3D;60941BFA-4B81-4A50-83DA-08D4B323CE4D;
            ##   907636A9-DED1-4030-B97B-08D4B27549CF;PermanentFailure;The message was undeliverable;NULL;2017-06-14 16:54:57.690;NULL;0;
            ##   2017-06-14 16:54:57.690;473847;NULL;2017-06-14 16:08:35.093
            sql_make_tbl = """CREATE TABLE {} (
              ID text,
              RowID integer,
              CampaignID text,
              MailoutID text,
              U3L_ReferenceID text,
              Status text,
              StatusMessage text,
              ReadDate text,
              BounceDate text,
              OptOutDate text,
              HasClicked bool,
              LastModifiedDate text,
              InstanceID text,
              Version text,
              DeliveryDate text
            )
            """.format(dest_tblname)
        elif fname.startswith('MailEvent'):
            dest_tblname = 'events'
            dest_fields = ['RowID', 'MessageID', 'Date', 'Event', 'LinkID',
                'RemoteIP', 'UserAgent', 'Source', 'MetaData',
                'ResendMessageID', 'CreateDate']
            ## 9;CF51B2E0-E3A6-4946-89B3-08D4B323E545;2017-06-14 12:52:56.260;ConsecutiveRead;NULL;103.232.180.27;
            ##   Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";
            ##   NULL;Other;CF51B2E0-E3A6-4946-89B3-08D4B323E545;2017-06-14 12:52:56.517
            sql_make_tbl = """CREATE TABLE {} (
              RowID integer,
              MessageID text,
              Date text,
              Event text,
              LinkID text,
              RemoteIP inet,
              UserAgent text,
              Source text,
              MetaData text,
              ResendMessageID text,
              CreateDate text
            )
            """.format(dest_tblname)
        elif fname.startswith('MailListData'):
            dest_tblname = 'optout'
            dest_fields = ['U3L_referenceID', 'U3L_CreateDate',
                'u3m_optoutdate']
            sql_make_tbl = """CREATE TABLE {} (
              U3L_referenceID text,
              U3L_CreateDate text,
              u3m_optoutdate text
            )
            """.format(dest_tblname)
        db.Pg.drop_tbl(con, cur, dest_tblname)
        cur.execute(sql_make_tbl)
        con.commit()
        db.Pg.csv2tbl(fpath, dest_tblname, dest_fields,
            dest_host=settings.PG_LOCAL_HOST,
            dest_port=settings.PG_LOCAL_PORT,
            dest_user=settings.PG_LOCAL_USER,
            dest_pwd=settings.PG_LOCAL_PWD,
            dest_db=DB_NAME,
            delim=';')
        print("Just ingested {} into local PostgreSQL instance".format(fname))

def indiv_engagement():
    """
    Join u3l_referenceid to events (so we can group by person),
    filter to relevant events, and see max aggregate count by week?
    """
    unused, cur = db.Pg.local(DB_NAME)
    sql_person_optout_mailoutids = """\
    SELECT DISTINCT ON (u3l_referenceid)
      logs.u3l_referenceid AS indiv_id,  -- person id has a different meaning in UbiquityLand (cross client id)
      logs.deliverydate,
      logs.mailoutid,
        99 AS  -- opt out trumps all
      event_rank
    FROM logs
    INNER JOIN
    optout
    USING(u3l_referenceid)
    WHERE logs.deliverydate <= optout.u3m_optoutdate
    ORDER BY u3l_referenceid, deliverydate DESC
    """
    sql_event_ranks = """\
    SELECT
      logs.u3l_referenceid AS indiv_id,
      logs.deliverydate,
      logs.mailoutid,
        CASE events.event
          WHEN 'OptedOut' THEN 99  -- opt out trumps all
          WHEN 'Read' THEN 2
          WHEN 'ConsecutiveRead' THEN 3
          WHEN 'LinkClicked' THEN 4
        END AS
      event_rank
    FROM events INNER JOIN logs
      ON events.messageid = logs.id
    """
    sql_max_event_rank = """\
    SELECT
      indiv_id,
      mailoutid,
        CASE
          WHEN MAX(event_rank) IN (1, 99) THEN 25 -- both are opt outs
          WHEN MAX(event_rank) = 2 THEN 50
          WHEN MAX(event_rank) = 3 THEN 75
          WHEN MAX(event_rank) = 4 THEN 100
          ELSE 0
        END AS
      score
    FROM (
      SELECT * 
      FROM (
        {sql_person_optout_mailoutids}
      ) AS person_optout_mailoutids
      UNION ALL
      {sql_event_ranks}
    ) AS src
    GROUP BY indiv_id, mailoutid
    """.format(sql_person_optout_mailoutids=sql_person_optout_mailoutids,
    sql_event_ranks=sql_event_ranks)
    cur.execute(sql_max_event_rank)
    data = cur.fetchall()
    scores = []
    scores_examples_dict = defaultdict(list)
    for n, row in enumerate(data, 1):
        if n <= 100:
            print(row)
        scores.append(row['score'])
        scores_examples_dict[row['score']].append(row)
    c = Counter(scores)
    for k, v in sorted(c.items()):
        print("{:>3} {:>7}".format(k, "{:,}".format(v)))
    ## get sample with one of each score
    SAMPLE_SIZE = 12
    for score in (0, 25, 50, 75, 100):
        score_examples = scores_examples_dict[score]
        try:
            examples4score = sample(score_examples, SAMPLE_SIZE)
        except ValueError:
            examples4score = score_examples
        print(score, examples4score)

def display_events():
    unused, cur = db.Pg.local(DB_NAME)
    sql_campaign_ids = """\
    SELECT DISTINCT campaignid
    FROM logs
    ORDER BY campaignid
    """
    cur.execute(sql_campaign_ids)
    campaign_ids = [row['campaignid'] for row in cur.fetchall()]
    sql_events = """\
    SELECT
      logs.campaignid,
      events.messageid,
        logs.u3l_referenceid AS
      personid,
      events.date,
      events.event
    FROM events INNER JOIN logs
      ON events.messageid = logs.id
    WHERE events.event IN ('Read', 'ConsecutiveRead', 'LinkClicked')
    AND logs.campaignid = %s
    ORDER BY u3l_referenceid, logs.id, events.date
    """
    for campaign_id in campaign_ids:
        cur.execute(sql_events, (campaign_id, ))
        data = cur.fetchall()
        prev_campaignid = None
        prev_messageid = None
        prev_personid = None
        for n, row in enumerate(data, 1):
            if row['campaignid'] != prev_campaignid:
                print("\nNew campaign - {} *********".format(row['campaignid']))
                print("Person - {}".format(row['personid']))
            else:
                if row['personid'] != prev_personid:
                    print("\nNew person - {}".format(row['personid']))
                    print("Message - {}".format(row['messageid']))
                else:
                    if row['messageid'] != prev_messageid:
                        print("New message - {}".format(row['messageid']))
                    
            print("{} {}".format(row['date'], row['event']))
            prev_campaignid = row['campaignid']
            prev_messageid = row['messageid']
            prev_personid = row['personid']
            if n > 1000:
                break

data_already_imported = True
if not data_already_imported:
    ingest_mail_csvs(ingest_path='/home/gps/projects/engagement/ingest/')

#display_events()
indiv_engagement()
