
import json
from pathlib import Path
from random import sample

from qat import db, mpl, settings, utils as qat_utils

import config
import utils

"""
Note - everything will run at a glacial speed unless we manually add indexes to
the key tables. Eventually Johannes will have to take responsibility for this so
it is part of a standard, repeatable process.

MailEvent messageid

MailLog id
MailLog u3l_referenceid
MailLog deliverydate

MailListData u3l_referenceid
MailListData u3m_optoutdate
MailListData u3m_optedout

I only wrap main() in a tunnel to make it easier to have non-colliding ports.
Also, why open and close lots of tunnels when we can have just one.
"""


class RawScores:

    RAW_SCORES_TBL = 'raw_scores'

    @staticmethod
    def _get_indiv_mailout_scores_sql(account_lbl,
            min_date_str=None, max_date_str=None):
        """
        People can have multiple types of events associated with a single
        mailout. So how are we giving them an overall score? We look for the
        furthest along the process they got with the exception that opting out
        trumps everything. It doesn't matter if you read emails or clicked on
        links if you opted out at the end. We'll consider you dis-engaged.

        Events types are given ranks and we just select the MAX rank per
        individual per mailout.

        Opting out is identified in two ways - by the standard opt-out event, or
        through another data source. The association with a mailout is arguably
        a little hairy but it is the best we can do given the structure of the
        data captured. And it is better to include those opt-outs than assume a
        link clicked to opt out represents the highest level of engagement
        rather than the lowest.
        """
        tbl_dets = utils.get_tbl_dets(account_lbl)
        WHERE_date_filter, AND_date_filter = utils.get_date_filters(
            min_date_str, max_date_str,
            lbl='mailout log', date_filter_fldname='logs.deliverydate')
        ## the main, events-based data
        sql_event_ranks = """\
        SELECT
          logs.u3l_referenceid AS indiv_id,
          logs.mailoutid,
          logs.deliverydate AS mailout_date,
            CASE events.event
              WHEN 'OptedOut' THEN 99  -- opt out trumps all
              WHEN 'Read' THEN 2
              WHEN 'ConsecutiveRead' THEN 3
              WHEN 'LinkClicked' THEN 4
            END AS
          event_rank
        FROM {events} AS events INNER JOIN {logs} AS logs
          ON events.messageid = logs.id
        {WHERE_date_filter}
        """.format(events=tbl_dets[config.MAIL_EVENT],
            logs=tbl_dets[config.MAIL_LOG],
            WHERE_date_filter=WHERE_date_filter)
        ## The external opt-out data per indiv - assumed they (almost always) opt out once per account.
        ## Opt outs taint all preceding mailouts for an indiv but we only look at the last
        ## Can't filter by date here or the tainting could affect mailout_ids it shouldn't (perhaps because we've removed the later ones that would have received the tainting).
        sql_person_optout_mailoutids = """\
        SELECT DISTINCT ON (u3l_referenceid)
          logs.u3l_referenceid AS indiv_id,  -- person id has a different meaning in UbiquityLand (it means a cross-business client person id)
          logs.mailoutid,
          logs.deliverydate AS mailout_date,
            99 AS  -- opt out trumps all
          event_rank
        FROM {logs} AS logs
        INNER JOIN
        {optout} AS optout
        USING(u3l_referenceid)
        WHERE logs.deliverydate <= optout.u3m_optoutdate
        AND optout.u3m_optedout
        """.format(logs=tbl_dets[config.MAIL_LOG],
            optout=tbl_dets[config.MAIL_LIST_DATA])
        sql_max_event_rank = """\
        SELECT
            '{account_lbl}' AS 
          account_lbl,
          indiv_id,
          mailoutid,
          mailout_date,
            CASE
              WHEN MAX(event_rank) = 99 THEN 25  -- whether an event opt out or from the separate opt out data
              WHEN MAX(event_rank) = 2 THEN 50
              WHEN MAX(event_rank) = 3 THEN 75
              WHEN MAX(event_rank) = 4 THEN 100
              ELSE 0
            END AS
          score
        FROM (
          {sql_event_ranks}
          UNION ALL
          {sql_person_optout_mailoutids} {AND_date_filter}
        ) AS src
        GROUP BY indiv_id, mailoutid, mailout_date
        ORDER BY indiv_id, mailout_date, mailoutid
        """.format(account_lbl=account_lbl,
            sql_person_optout_mailoutids=sql_person_optout_mailoutids,
            sql_event_ranks=sql_event_ranks,
            AND_date_filter=AND_date_filter)
        return sql_max_event_rank

    @staticmethod
    def store_raw_scores(account_lbl, *, local_port,
            min_date_str=None, max_date_str=None,
            clear_first=True):
        sql_scores = RawScores._get_indiv_mailout_scores_sql(
            account_lbl, min_date_str, max_date_str)
        con, cur = utils.Db.get_con_cur(local_port)
        exists = utils.Db.table_exists(
            cur, RawScores.RAW_SCORES_TBL, schema='public')
        if not exists:
            sql_make_tbl = """\
            CREATE TABLE {} (
              account_lbl TEXT,
              indiv_id TEXT,
              mailoutid TEXT,
              mailout_date TIMESTAMP,
              score INTEGER
            )
            """.format(RawScores.RAW_SCORES_TBL)
            cur.execute(sql_make_tbl)
            con.commit()
            print("Created '{}'".format(RawScores.RAW_SCORES_TBL))
        else:
            if clear_first:
                if min_date_str or max_date_str:
                    qat_utils.warn("Trying to add data filtered by date yet "
                        "nuking first")
                    input("Are you sure? Red button (GUI) or Ctrl-c (CLI) to "
                        "quit or any key to continue: ")
                ## Nuke content for that account
                sql_del = """\
                DELETE FROM {}
                WHERE account_lbl = %s
                """.format(RawScores.RAW_SCORES_TBL)
                cur.execute(sql_del, (account_lbl, ))
                con.commit()
                print("Deleted records from '{}'".format(
                    RawScores.RAW_SCORES_TBL))
        sql_append = """\
        INSERT INTO {raw_scores_tbl}
        SELECT *
        FROM ({sql_scores}) AS scores
        """.format(raw_scores_tbl=RawScores.RAW_SCORES_TBL,
            sql_scores=sql_scores)
        print(sql_append)
        cur.execute(sql_append)
        con.commit()
        cur.close()
        con.close()
        print("Finished updating '{}' with data for '{}'".format(
            RawScores.RAW_SCORES_TBL, account_lbl))

    @staticmethod
    def get_avg_scores(account_lbl, *, local_port, 
            min_date_str=None, max_date_str=None):
        debug = False
        _WHERE_date_filter, AND_date_filter = utils.get_date_filters(
            min_date_str, max_date_str,
            lbl='engagement score', date_filter_fldname='mailout_date')
        con, cur = utils.Db.get_con_cur(local_port)
        sql_avg_scores = """\
        SELECT
          indiv_id,
            AVG(score)::float AS
          avg_score
        FROM {raw_scores}
        WHERE account_lbl = %s
        {AND_date_filter}
        GROUP BY indiv_id
        ORDER BY indiv_id
        """.format(raw_scores=RawScores.RAW_SCORES_TBL,
            AND_date_filter=AND_date_filter)
        cur.execute(sql_avg_scores, (account_lbl, ))
        avg_scores_data = cur.fetchall()
        if debug:
            for n, row in enumerate(avg_scores_data, 1):
                print(row)
                if n > 200:
                    break
        cur.close()
        con.close()
        return avg_scores_data


class Spending:

    """
    On on-premises Isilon cluster NOT AWS.
    """

    @staticmethod
    def ingest_lotto_spending():
        dest_tblname = config.LOTTO_PURCHASE_DATA
        fname = '003ac39b2c8e40f9833008d27638c067.csv'
        fpath = Path('~') / 'projects' / 'engagement' / 'storage' / 'Lotto' / fname
        con, cur = db.Pg.local(config.LOCAL_ENGAGEMENT_DB)
        db.Pg.drop_tbl(con, cur, dest_tblname)
        sql_make_tbl = """\
        CREATE TABLE {} (
          U3L_ID TEXT,
          U3L_TransactionID TEXT,
          u3l_referenceid TEXT,
          U3L_CreateDate TEXT,
          U3L_LastModified TEXT,
          U3L_Source TEXT,
          U3L_Version TEXT,
          lottotransactionid TEXT,
          purchasedate TEXT,
          purchasetime TEXT,
          product TEXT,
          purchasemethod TEXT,
          diptype TEXT,
          channel TEXT,
          purchaseamount FLOAT,
          walletbalance TEXT
        )
        """.format(dest_tblname)
        dest_fields = ('U3L_ID', 'U3L_TransactionID', 'U3L_ReferenceID',
            'U3L_CreateDate', 'U3L_LastModified', 'U3L_Source', 'U3L_Version',
            'lottotransactionid', 'purchasedate', 'purchasetime', 'product',
            'purchasemethod', 'diptype', 'channel', 'purchaseamount',
            'walletbalance')
        cur.execute(sql_make_tbl)
        con.commit()
        db.Pg.csv2tbl(fpath, dest_tblname, dest_fields,
            dest_host=settings.PG_LOCAL_HOST,
            dest_port=settings.PG_LOCAL_PORT,
            dest_user=settings.PG_LOCAL_USER,
            dest_pwd=settings.PG_LOCAL_PWD,
            dest_db=config.LOCAL_ENGAGEMENT_DB,
            delim=';')
        print("Just ingested {} into local PostgreSQL instance".format(fname))

    @staticmethod
    def get_spending_data(account_lbl):
        debug = False
        tbl_dets = utils.get_tbl_dets(account_lbl)
        sql_spending = tbl_dets[config.SQL_SPENDING]
        con, cur = db.Pg.local(config.LOCAL_ENGAGEMENT_DB)
        cur.execute(sql_spending)
        spending_data = cur.fetchall()
        if debug:
            for n, row in enumerate(spending_data, 1):
                print(row)
                if n > 200:
                    break
        cur.close()
        con.close()
        return spending_data


class Correlation:

    @staticmethod
    def get_cache_fname(account_lbl):
        return "{}_avg_scores_tot_dollars.json".format(account_lbl)

    @staticmethod
    def get_avg_scores_tot_dollars_data(account_lbl, *, local_port,
            min_date_str=None, max_date_str=None,
            use_cached=False):
        """
        Start with indivs we have purchase data for and then get avg score
        (if any).
        """
        cache_fname = Correlation.get_cache_fname(account_lbl)
        if not use_cached:
            spending_data = Spending.get_spending_data(account_lbl)
            avg_scores_data = RawScores.get_avg_scores(
                account_lbl, local_port=local_port,
                min_date_str=min_date_str, max_date_str=max_date_str)
            avg_scores_dict = dict(avg_scores_data)
            avg_scores_tot_dollars = []
            for indiv_id, tot_dollars in spending_data:
                try:
                    avg_score = avg_scores_dict[indiv_id]
                except KeyError:
                    continue
                else:
                    avg_scores_tot_dollars.append(
                        (indiv_id, avg_score, tot_dollars))
            with open(cache_fname, 'w') as f:
                json.dump(avg_scores_tot_dollars, f)
        with open(cache_fname) as f:
            avg_scores_tot_dollars = json.load(f)
        return avg_scores_tot_dollars

    @staticmethod
    def examine_sample(account_lbl, *, local_port):
        con_local, cur_local = db.Pg.local(config.LOCAL_ENGAGEMENT_DB)
        con_aws, cur_aws = utils.Db.get_con_cur(local_port)
        cache_fname = Correlation.get_cache_fname(account_lbl)
        with open(cache_fname) as f:
            avg_scores_tot_dollars = json.load(f)
        sql_engagement = """\
        SELECT mailoutid, mailout_date, score
        FROM {}
        WHERE indiv_id = %s
        ORDER BY mailoutid
        """.format(RawScores.RAW_SCORES_TBL)
        sql_spending = """\
        SELECT purchasedate, purchasetime, purchaseamount
        FROM {}
        WHERE u3l_referenceid = %s
        ORDER BY purchasedate, purchasetime
        """.format(config.LOTTO_PURCHASE_DATA)
        q1 = '<= 25'
        q2 = '> 25, <= 50'
        q3 = '> 50, <= 75'
        q4 = '> 75'
        qs = [
            (q1, lambda x: x <= 25),
            (q2, lambda x: 25 < x <= 50),
            (q3, lambda x: 50 < x <= 75),
            (q4, lambda x: x > 75),
        ]
        for q, fn in qs:
            data = [(indiv_id, avg_score, tot_dollars)
                for indiv_id, avg_score, tot_dollars in avg_scores_tot_dollars
                if fn(avg_score)]
            print("\nSample of {}".format(q))
            sampled_data = sample(data, 5)
            for indiv_id, avg_score, tot_dollars in sampled_data:
                print("{:<25} {:>5} {:>8}".format(
                    indiv_id, round(avg_score, 1), tot_dollars))
            cur_aws.execute(sql_engagement, (indiv_id, ))
            score_data = cur_aws.fetchall()
            for mailoutid, mailout_date, score in score_data:
                print("    {:<20} {:<20} {:>16}".format(
                    mailoutid, mailout_date, score))
            cur_local.execute(sql_spending, (indiv_id, ))
            spending_data = cur_local.fetchall()
            for purchasedate, purchasetime, purchaseamount in spending_data:
                print("    {:<16} {:<16} {:>16}".format(
                    purchasedate, purchasetime, purchaseamount))
        cur_local.close()
        con_local.close()
        cur_aws.close()
        con_aws.close()


LOCAL_PORT = 9999

@utils.Db.tunnel(LOCAL_PORT)
def main():
    input_data_to_process = False
    get_fresh_outputs = True

    account_lbl = config.LOTTO_LBL
    
    Correlation.examine_sample(account_lbl, local_port=LOCAL_PORT); return

    if input_data_to_process:
        RawScores.store_raw_scores(
            account_lbl, local_port=LOCAL_PORT, clear_first=True)
        Spending.ingest_lotto_spending()
        return

    if get_fresh_outputs:
        avg_scores_tot_dollars = Correlation.get_avg_scores_tot_dollars_data(
            account_lbl, local_port=LOCAL_PORT,
            min_date_str='2018-02-01', max_date_str='2018-03-01',
            use_cached=get_fresh_outputs)

    xys = [(x, y) for _indiv_id, x, y in avg_scores_tot_dollars]
    mpl.Chart.view_quick_scatterplot(
        xys=xys, opacity=0.03,
        title='Score driving dollars?',
        xlbl='Average engagement score', ylbl='Purchase amount $',
        force_y0=True)

if __name__ == '__main__':
    main()
    
