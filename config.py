
import os

from dotenv import load_dotenv, find_dotenv
dot_env_path = find_dotenv()  ## Search in increasingly higher folders for the given file
load_dotenv(find_dotenv())

SSH_BASTION_ENDPOINT = '13.211.116.116'  ## was 'ec2-13-210-205-139.ap-southeast-2.compute.amazonaws.com'
SSH_BASTION_USER = 'ec2-user'
SSH_BASTION_PKEY = "/home/gps/cip-aws.pem"
INTERNAL_AWS_ENDPOINT = 'gps-engagement.ch4t86zhcpuo.ap-southeast-2.rds.amazonaws.com'  ## only reachable through the bastion

LOCAL_ENGAGEMENT_DB = 'engage'

TBL_SUFFIX = 'Table name suffix'
SQL_FIN_TRANSACTIONS = 'SQL financial transactions'

LOTTO_LBL = 'Lotto'
LOTTO_TBL_SUFFIX = '81be798c6b26418bab1808cf6858c198'
LOTTO_PURCHASE_DATA = 'lotto_purchase_data'
LOTTO_SQL_SPENDING = """\
SELECT
  indiv_id,
    SUM(dollars) AS
  tot_dollars
FROM (
  SELECT
    SUBSTRING(u3l_referenceid, 2, 36) AS indiv_id, -- uuids received with enclosing curly braces so need to remove to be consistent with PG data
    purchasedate AS date_time_str,
    purchaseamount AS dollars
  FROM {}
) AS src
GROUP BY indiv_id
ORDER BY indiv_id
""".format(LOTTO_PURCHASE_DATA)


## dual purpose - a key and a table name prefix
MAIL_LOG = 'MailLog'
MAIL_EVENT = 'MailEvent'
MAIL_LIST_DATA = 'ListData'

SQL_SPENDING = 'SQL transactions'

CONFIG = {
    LOTTO_LBL: {
        TBL_SUFFIX: LOTTO_TBL_SUFFIX,
        SQL_SPENDING: LOTTO_SQL_SPENDING,
    }
}

DB_PASSWORD = os.getenv('ENGAGEMENT_AWS_PWD')
LOCAL_PORT = 5439
ENGAGE_DB_KWARGS = {
    'dbname': 'engagement',
    'user': 'engage_admin',
    'host': 'localhost',
    'port': LOCAL_PORT,
    'password': DB_PASSWORD,
}
