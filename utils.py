
from functools import wraps
import os

import psycopg2 as pg
import psycopg2.extras as pg_extras
from sshtunnel import HandlerSSHTunnelForwarderError, SSHTunnelForwarder

from qat import utils as qat_utils

import config
CONFIG = config.CONFIG


class Db:

    @staticmethod
    def tunnel(local_port=config.LOCAL_PORT):
        def tunnel_decorator(fn):
            @wraps(fn)
            def fn_wrapped_in_ssh_tunnel(*args, **kwargs):
                ssh_private_key_password = os.getenv('AWS_CIP_PWD')
                try:
                    with SSHTunnelForwarder(
                        ssh_address_or_host=config.SSH_BASTION_ENDPOINT,  ## the bastion server (perhaps surprisingly is not the bit between the ports in the port forwarding)
                        ssh_username=config.SSH_BASTION_USER,
                        ssh_pkey=config.SSH_BASTION_PKEY,
                        ssh_private_key_password=ssh_private_key_password,
                        remote_bind_address=(config.INTERNAL_AWS_ENDPOINT, 5432),
                        local_bind_address=('localhost', local_port)  ## will fail if you have bound to this before and not closed that tunnel i.e. only idempotent within context manager. nmap -p1-65335 localhost tells all ;-)
                        ) as _server:

                        fn(*args, **kwargs)
                except HandlerSSHTunnelForwarderError as e:
                    print("Unable to open fresh tunnel using local port - "
                        "original error: {}\nMoving to next step as if tunnel "
                        "is already open. No guarantees of success though as "
                        "tunnel may actually be down.".format(e))
                    fn(*args, **kwargs)
            return fn_wrapped_in_ssh_tunnel
        return tunnel_decorator

    @staticmethod
    def get_con_cur(local_port=config.LOCAL_PORT):
        """
        Get connection and cursor for engagement database.
    
        Assumes tunnel is open. So should only be called inside a function
        decorated with tunnel.
        """
        kwargs = config.ENGAGE_DB_KWARGS.copy()
        kwargs['port'] = local_port
        con = pg.connect(**kwargs)
        cur = con.cursor(cursor_factory=pg_extras.DictCursor)
        return con, cur

    @staticmethod
    def table_exists(cur, tblname, schema='public'):
        """
        Does a table exist already or not? Note - case matters. A table
        might not exist but still block a table from being made with a name
        differing by case only. E.g.
        matrix_areas_hamilton_weekday_2017 vs matrix_areas_Hamilton_Weekday_2017

        :param dbapi.cursor cur: cursor
        :param string tblname: table name
        :param string schema: schema
        :return: table exists or not
        :rtype: bool
        """
        ## https://stackoverflow.com/questions/20582500/how-to-check-if-a-table-exists-in-a-given-schema
        sql = """\
        SELECT EXISTS (
        SELECT 1 
        FROM   pg_tables
        WHERE  schemaname = '{schema}'
        AND    tablename = '{tblname}'
        ) AS exists
        """.format(tblname=tblname, schema=schema)
        cur.execute(sql)
        exists = cur.fetchone()['exists']
        return exists


def get_tbl_dets(account_lbl):
    tbl_dets = {}
    tbl_dets[config.MAIL_LOG] = '{}_{}'.format(
        config.MAIL_LOG, CONFIG[account_lbl][config.TBL_SUFFIX])
    tbl_dets[config.MAIL_EVENT] = '{}_{}'.format(
        config.MAIL_EVENT, CONFIG[account_lbl][config.TBL_SUFFIX])
    tbl_dets[config.MAIL_LIST_DATA] = '{}_{}'.format(
        config.MAIL_LIST_DATA, CONFIG[account_lbl][config.TBL_SUFFIX])
    tbl_dets[config.SQL_SPENDING] = (
        CONFIG[account_lbl][config.SQL_SPENDING])
    return tbl_dets

def get_date_filters(min_date_str, max_date_str, *, lbl, date_filter_fldname):
    if min_date_str or max_date_str:
        if min_date_str and max_date_str:
            qat_utils.warn("Limiting {} data to between '{}' and '{}'"
                .format(lbl, min_date_str, max_date_str))
            date_filter = ("{} BETWEEN "
                "'{} 00:00:00' AND '{} 23:59:59'"
                .format(date_filter_fldname, min_date_str, max_date_str))
            WHERE_date_filter = ("WHERE {}".format(date_filter))
            AND_date_filter = ("AND {}".format(date_filter))
        else:
            qat_utils.warn("Only supplied one date so no filtering applied to "
                "{} data".format(lbl))
    else:
        WHERE_date_filter = ''
        AND_date_filter = ''
    return WHERE_date_filter, AND_date_filter
